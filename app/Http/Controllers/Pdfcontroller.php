<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PDF;
use TCPDF_FONTS;

class Pdfcontroller extends Controller
{
    public function index()
    {
        $fontname = TCPDF_FONTS::addTTFfont(public_path('/fonts/Roboto-Medium.ttf'), 'TrueTypeUnicode', '', 32);
        PDF::SetFont($fontname, '', 14, true);
        //dd(public_path('\font\Roboto-Medium.ttf'));
        PDF::SetTitle('Hello World');

        PDF::setHeaderCallback(function($pdf) {
           // $pdf->SetY(0);
            //$pdf->Image('https://wikilaptop.com/wp-content/uploads/2021/01/1610534235_Tong-hop-100-Background-dep-nhat.jpg', 0, 0, 210, 15, '', '', '', false, 300, '', false, false, 0);
            // view blade
            $pdf->SetXY(10,0);
            $pdf->Cell(0, 10, 'Page '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'L', 0, '', 0, false, 'T', 'M');
            $pdf->SetXY(-50,0);
            $pdf->Cell(0, 10, 'Page '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });

        PDF::setFooterCallback(function($pdf) {

            // Position at 15 mm from bottom
            $pdf->SetY(-15);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(0, 10, 'Page '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

        });

        PDF::AddPage();
        // disable auto-page-break
        PDF::SetAutoPageBreak(false, 0);// get the current page break margin
        $bMargin = PDF::getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = PDF::getAutoPageBreak();
        PDF::Image(public_path('image_demo.jpg'), 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
        PDF::SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        PDF::setPageMark();
        PDF::SetXY(10, 100);
        $html = '<span style="text-align:center;font-weight:bold;font-size:80pt;position: relative;padding-top: 200px">Quyền anh 3</span>';
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::SetAutoPageBreak(true, 0);
        PDF::SetMargins(20, 20, 10);
        // Print a page blank
        PDF::AddPage();
        // view blade
        $view = \View::make('welcome');
        $html_content = $view->render();
        PDF::writeHtml($html_content, false, false, true, false, '');



        $svgRaw = '<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="500" height="350">
                                            <g></g>
                                            <line x1="35" y1="295" x2="250" y2="35" stroke-linecap="round" stroke-width="3" stroke="#9c27b0"></line>
                                            <line x1="250" y1="35" x2="465" y2="295" stroke-linecap="round" stroke-width="3" stroke="#9c27b0"></line>
                                            <line x1="35" y1="295" x2="465" y2="295" stroke-linecap="round" stroke-width="3" stroke="#9c27b0"></line>
                                            <line x1="35" y1="295" x2="250" y2="110" stroke-linecap="round" stroke-width="3" stroke="#9c27b0"></line>
                                            <line x1="250" y1="110" x2="465" y2="295" stroke-linecap="round" stroke-width="3" stroke="#9c27b0"></line>
                                            <line x1="125" y1="295" x2="125" y2="220" stroke-linecap="round" stroke-width="3" stroke="#9c27b0"></line>
                                            <line x1="125" y1="220" x2="250" y2="295" stroke-linecap="round" stroke-width="3" stroke="#9c27b0"></line>
                                            <line x1="250" y1="295" x2="375" y2="220" stroke-linecap="round" stroke-width="3" stroke="#9c27b0"></line>
                                            <line x1="375" y1="220" x2="375" y2="295" stroke-linecap="round" stroke-width="3" stroke="#9c27b0"></line>
                                            <line x1="250" y1="35" x2="250" y2="110" stroke-linecap="round" stroke-width="3" stroke="#9c27b0"></line>
                                            <circle r="17.5" cx="250" cy="35" fill="#ebbcff" stroke-width="1" stroke="#9c27b0"></circle>
                                            <text font-size="13" font-family="segoe UI" font-weight="bold" x="246" y="24.109375">
                                                <tspan dy="16.900000000000002" x="246">6</tspan>
                                            </text>
                                            <circle r="17.5" cx="35" cy="295" fill="#ebbcff" stroke-width="1" stroke="#9c27b0"></circle>
                                            <text font-size="13" font-family="segoe UI" font-weight="bold" x="31.2578125" y="284.109375">
                                                <tspan dy="16.900000000000002" x="31.2578125">2</tspan>
                                            </text>
                                            <circle r="17.5" cx="465" cy="295" fill="#ebbcff" stroke-width="1" stroke="#9c27b0"></circle>
                                            <text font-size="13" font-family="segoe UI" font-weight="bold" x="461" y="284.109375">
                                                <tspan dy="16.900000000000002" x="461">8</tspan>
                                            </text>
                                            <circle r="17.5" cx="125" cy="220" fill="#ebbcff" stroke-width="1" stroke="#9c27b0"></circle>
                                            <text font-size="13" font-family="segoe UI" font-weight="bold" x="121.2578125" y="209.109375">
                                                <tspan dy="16.900000000000002" x="121.2578125">2</tspan>
                                            </text>
                                            <circle r="17.5" cx="250" cy="110" fill="#ebbcff" stroke-width="1" stroke="#9c27b0"></circle>
                                            <text font-size="13" font-family="segoe UI" font-weight="bold" x="246.2578125" y="99.109375">
                                                <tspan dy="16.900000000000002" x="246.2578125">2</tspan>
                                            </text>
                                            <circle r="17.5" cx="375" cy="220" fill="#ebbcff" stroke-width="1" stroke="#9c27b0"></circle>
                                            <text font-size="13" font-family="segoe UI" font-weight="bold" x="371.5" y="209.109375">
                                                <tspan dy="16.900000000000002" x="371.5">4</tspan>
                                            </text>
                                            <circle r="17.5" cx="250" cy="295" fill="#ebbcff" stroke-width="1" stroke="#9c27b0"></circle>
                                            <text font-size="13" font-family="segoe UI" font-weight="bold" x="246.5" y="284.109375">
                                                <tspan dy="16.900000000000002" x="246.5">4</tspan>
                                            </text>
                                            <circle r="17.5" cx="125" cy="295" fill="#ebbcff" stroke-width="1" stroke="#9c27b0"></circle>
                                            <text font-size="13" font-family="segoe UI" font-weight="bold" x="121.2578125" y="284.109375">
                                                <tspan dy="16.900000000000002" x="121.2578125">2</tspan>
                                            </text>
                                            <circle r="17.5" cx="375" cy="295" fill="#ebbcff" stroke-width="1" stroke="#9c27b0"></circle>
                                            <text font-size="13" font-family="segoe UI" font-weight="bold" x="371" y="284.109375">
                                                <tspan dy="16.900000000000002" x="371">8</tspan>
                                            </text>
                                            <text font-size="13" font-family="segoe UI" font-weight="bold" x="35" y="312">
                                                <tspan dy="16.900000000000002" x="35"></tspan>
                                            </text>
                                            <text font-size="13" font-family="segoe UI" font-weight="bold" x="117.515625" y="320">
                                                <tspan dy="16.900000000000002" x="117.515625">11</tspan>
                                            </text>
                                            <text font-size="13" font-family="segoe UI" font-weight="bold" x="242.515625" y="320">
                                                <tspan dy="16.900000000000002" x="242.515625">13</tspan>
                                            </text>
                                            <text font-size="13" font-family="segoe UI" font-weight="bold" x="359.78125" y="320">
                                                <tspan dy="16.900000000000002" x="359.78125">1997</tspan>
                                            </text>
                                            <text font-size="13" font-family="segoe UI" font-weight="bold" x="465" y="312">
                                                <tspan dy="16.900000000000002" x="465"></tspan>
                                            </text>
                                        </svg>';
        PDF::ImageSVG('@' . $svgRaw, $x=15, $y=30, $w='', $h='', $link='', $align='', $palign='', $border=0, $fitonpage=false);

        PDF::Write(0, $txt='', '', 0, 'L', true, 0, false, false, 0);
        PDF::Output(uniqid() . '_SamplePDF.pdf');
    }
}
